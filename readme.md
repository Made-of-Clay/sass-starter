# Sass Starter

This setup is meant to save me time during project start-ups. There is no *styles.css*
file created initially. It must be compiled.

```scss
// from /css directory
sass --update scss/styles.scss:styles.css
```

This command will generate the first file (assuming the import modules rule doesn't error).

## Bourbon et al.

Bourbon, Neat, & Bitters need to be installed.

### Install Bourbon

From the terminal, while in /css/scss directory:

```
bourbon install
```

### Install Neat

From the terminal, while in /css/scss directory:

```
neat install
```

### Install Bitters

From the terminal, while in /css/scss directory:

```
bitters install
```